import sys
import tensorflow as tf
from tensorflow import keras
import numpy as np

def build_model():
	model = tf.keras.models.Sequential([
		tf.keras.layers.Dense(17, activation='relu'),
		tf.keras.layers.Dense(68, activation='relu'),
		tf.keras.layers.Dense(272, activation='relu'),
		tf.keras.layers.Dense(5, activation='softmax'),
		tf.keras.layers.Dense(1, activation='sigmoid')
	])
	
	optimizer=tf.train.AdamOptimizer(0.001)
	model.compile(optimizer, loss='mean_squared_error',	metrics=['accuracy'])
	model.load_weights('./checkpoint6')
	return model

# x = np.array([np.array(sys.argv[1:]).astype(np.float)])
model = build_model()

x = np.empty([1,17])
while True:
	x[0] = [float(i) for i in input().split()]
	tf.keras.backend.get_session();
	y = model.predict(x, batch_size=1)
	print(y[0][0])
	x = np.empty([1,17])