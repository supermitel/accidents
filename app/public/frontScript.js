var $ = require('jquery');

var socket = io();

socket.on('welcome', function(data) {
    console.log(data.message);

    // Respond with a message including this clients' id sent from the server
    socket.emit('i am client', {data: 'foo!', id: data.id});
});
// socket.on('time', function(data) {
//     addMessage(data.time);
// });
socket.on('error', console.error.bind(console));
socket.on('message', console.log.bind(console));

socket.on('prediction', (data) =>{
    console.log(data);
    $("#riskIndicator").text(data);
    $('#riskBar').val(data);
})

function getData(){
    data = [];
    for(i = 0; i < 17; i++)
    {
        var element = $("*[data-index='"+i+"']");
        if(element.attr('type')==='radio')
            data[i] = element.prop('checked')?1:0;
        else data[i] = element.val();
    }
    return data;
}

$(document).ready(()=>{
    // console.log("started");
    // setInterval(()=>console.log(getData()), 1000);
    $("#panel").on("input", ":input", (e)=>{
        $(e.currentTarget).siblings().val($(e.currentTarget).val());
        socket.emit('data', getData());
    })
    
    socket.emit('data', getData());
})