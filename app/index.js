var http = require('http')
var fs = require('fs')
var express = require('express');
var app = express();
var path = require('path')

const child_process = require('child_process');

var server = app.listen(3030, function () {
    console.log('Example app listening on port 3030!');
  });
    
  
var io = require('socket.io').listen(server);

app.get('/', function (req, res) {
    res.sendFile(path.resolve('public/index.html'));
});

app.use(express.static(__dirname + '/public'));  

io.on('connection', function(socket) {
    // Use socket to communicate with this particular client only, sending it it's own id
    socket.emit('welcome', { message: 'Welcome!', id: socket.id });

    socket.on('i am client', console.log);

    proc = child_process.spawn("python3", [path.resolve("predictions.py")]);
    proc.stdout.on('data', (data)=>{
        console.log(data.toString());
        socket.emit("prediction", data.toString());
    })
    proc.stdin.setEncoding('utf-8');
    
    proc.stderr.on('data', (data)=>{
        console.log(data.toString());
    })

    socket.on('data', (data)=>{
        var s = "";
        for(var i = 0; i < data.length; i++)
        {
            s = s+data[i]+" ";
        }
        s+="\n";
        console.log(s);
        proc.stdin.write(s);
    })
});




