import numpy as np
from pprint import pprint


rawData = np.genfromtxt("date.csv", dtype=None, delimiter="\t", encoding="UTF-8");
rawData = np.delete(rawData, 0, 0);

linearizeC9 = {" ":0, "None": 0, "Low": 1./3, "Moderate": 2./3, "High": 1}

np.random.shuffle(rawData);

def dataFunction(x):
    return np.array([float(x[1]), float(x[2]), float(x[3]), float(x[4]), float(x[5]), float(x[6]), float(x[7]), int(x[8] == "clear"), int(x[8] == "rain"), int(x[8] == "snow"), float(linearizeC9[x[9]]), float(x[10]), float(x[11]), int(x[12] == "Dry"), int(x[12] == "Wet"), int(x[12] == "Snow covered"), int(x[12] == "Visible tracks")])

data = [];
result = [];

deletedLines = 0;
deletedWithAccidents = 0;

for pos in range (len(rawData)):
    try:
        data.append(dataFunction(rawData[pos]))
        result.append(int(rawData[pos][13] == "Yes"))
    except:
        deletedLines += 1;
        deletedWithAccidents += 1 if int(rawData[pos][13] == "Yes") else 0

np.save("dat", data);
np.save("res", result);

print("saved\n", "deletedLines: ", deletedLines, "\nfrom wich accidents: ", deletedWithAccidents);
unique, counts = np.unique(result, return_counts=True)

print("final data results: ");
print(dict(zip(unique, counts)));